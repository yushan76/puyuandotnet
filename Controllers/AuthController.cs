﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace PuyuanDotNet7.Controllers;


[Route("api/[controller]")]
[ApiController]
[AllowAnonymous]
public class AuthController : ControllerBase
{
    private readonly AuthService _authService;
    
    public AuthController(AuthService authService)
    {
        _authService = authService;
    }

    /// <summary>
    /// 2. 登入
    /// </summary>
    /// <param name="login"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<IActionResult> Login(LoginDto login)
    {
        if (login == null) 
        {
            return BadRequest();
        }
        var result = await _authService.Login(login);
        return result;
    }
}
