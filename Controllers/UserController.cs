﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PuyuanDotNet7.Services;

namespace PuyuanDotNet7.Controllers;


[Route("api/[controller]")]
[ApiController]

public class UserController : ControllerBase
{
    private readonly UserService _userService;

    public UserController(UserService userService)
    {
        _userService = userService;
    }

    /// <summary>
    /// 7.個人資訊設定
    /// </summary>
    /// <param name="user"></param>
    /// <returns></returns>
    [HttpPost]
    [Authorize]
    public async Task<IActionResult> UserInfo(UserDto user)
    {
        if (user == null)
        {
            return BadRequest();
        }
        // 連接資料表
        string uuid = User.Claims.First(e => e.Type.Equals("jti")).Value;
        var result = await _userService.UserInfo(user, uuid);
        return result;
    }
}