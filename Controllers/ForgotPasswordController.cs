﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PuyuanDotNet7.Services;

namespace PuyuanDotNet7.Controllers;

[ApiController]
[Route("api/[controller]")]
[AllowAnonymous]
public class forgotPasswordController : ControllerBase
{
    private readonly ForgotPasswordService _forgotPasswordService;

    public forgotPasswordController(ForgotPasswordService forgotPasswordService)
    {
        _forgotPasswordService = forgotPasswordService;
    }

    // <summary>
    // 5.忘記密碼
    // </summary>
    // <param name="ForgotPassword"></param>
    // <returns></returns>
    [HttpPost]
    public async Task<IActionResult> ForgotPassword(ForgotPasswordDto forgotPassword)
    {
        if (forgotPassword == null)
        {
            return BadRequest();
        }
        var result = await _forgotPasswordService.ForgotPassword(forgotPassword);
        return result;
    }
}
