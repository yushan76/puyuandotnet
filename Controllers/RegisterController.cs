﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace PuyuanDotNet7.Controllers;

[ApiController]
[Route("api/[controller]")]
[AllowAnonymous]
public class RegisterController : ControllerBase
{
    private readonly RegisterService _registerService;

    public RegisterController(RegisterService registerService)
    {
        _registerService= registerService;
    }

    /// <summary>
    /// 1. 註冊
    /// </summary>
    /// <param name="register"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<IActionResult> Register(RegisterDto register)
    {
        if (register == null)
        {
            return BadRequest("檢查輸入資料");
        }
        var result = await _registerService.Register(register);
        return result;
    }
}
