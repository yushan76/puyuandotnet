using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace PuyuanDotNet7.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class UpdateBloodPressureController : ControllerBase
    {
        private readonly UpdateBloodPressureService _updateBloodPressureService;

        public UpdateBloodPressureController(UpdateBloodPressureService updateBloodPressureService)
        {
            this._updateBloodPressureService = updateBloodPressureService;
        }
        // <summary>
        // 8. 上傳血壓測量結果
        // </summary>
        // <param name="bloodpressure"></param>
        // <returns></returns>

        [HttpPost]
        public async Task<IActionResult> BloodPressure(UpdateBloodPresureDto bloodpressure)
        {
            var UUID = User.Claims.First(claim => claim.Type == "JWT_JTI").Value;
            if (bloodpressure == null)
            {
                return BadRequest("檢查輸入資料");
            }
            var result = await _updateBloodPressureService.BloodPressure(bloodpressure, UUID);
            return result;
        }
    }
}