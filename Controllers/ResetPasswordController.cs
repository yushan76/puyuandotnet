﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PuyuanDotNet7.Services;

namespace PuyuanDotNet7.Controllers;

[ApiController]
[Route("api/[controller]")]
[AllowAnonymous]
public class resetPasswordController : ControllerBase
{
    private readonly ResetPasswordService _resetPasswordService;

    public resetPasswordController(ResetPasswordService resetPasswordService)
    {
        _resetPasswordService = resetPasswordService;
    }

    // <summary>
    // 6.重設密碼
    // </summary>
    // <param name="ResetPassword"></param>
    // <returns></returns>
    [HttpPost]
    public async Task<IActionResult> ResetPassword(ResetPasswordDto resetPassword)
    {
        if (resetPassword == null)
        {
            return BadRequest();
        }
        var result = await _resetPasswordService.ResetPassword(resetPassword);
        return result;
    }
}
