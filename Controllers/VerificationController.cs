﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace PuyuanDotNet7.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class VerificationController : ControllerBase
    {
        private readonly VerificationService _verificationService;

        public VerificationController(VerificationService verificationService)
        {
            _verificationService = verificationService;
        }

        /// <summary>
        /// 3. 發送驗證碼
        /// </summary>
        /// <param name="sendVerification"></param>
        /// <returns></returns>
        [HttpPost("send")]
        public async Task<IActionResult> SendVerification(SendVerificationDto sendVerification)
        {
            if (sendVerification == null)
            {
                return BadRequest("檢查輸入資料");
            }
            var result = await _verificationService.SendVerification(sendVerification);
            return result;
        }

        /// <summary>
        /// 4.檢查驗證碼
        /// </summary>
        /// <param name="checkVerification"></param>
        /// <returns></returns>
        [HttpPost("check")]
        public async Task<IActionResult> CheckVerification(CheckVerificationDto checkVerification)
        {
            if (checkVerification == null)
            {
                return BadRequest("檢查輸入資料");
            }
            var result = await _verificationService.CheckVerification(checkVerification);
            return result;
        }

        
    }
}
