﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PuyuanDotNet7.Services;

namespace PuyuanDotNet7.Controllers;

[ApiController]
[Route("api/[controller]")]
[AllowAnonymous]
public class defaultController : ControllerBase
{
    private readonly DefaultService _defaultService;

    public defaultController(DefaultService defaultService)
    {
        _defaultService = defaultService;
    }

    // <summary>
    // 5.個人預設值
    // </summary>
    // <param name="Default"></param>
    // <returns></returns>
    [HttpPost]
    public async Task<IActionResult> DefaultInfo(DefaultDto DefaultDto)
    {
        if (DefaultDto == null)
        {
            return BadRequest();
        }
        string uuid = User.Claims.First(e => e.Type.Equals("jti")).Value;
        var result = await _defaultService.DefaultInfo(DefaultDto, uuid);
        return result;
    }
}
