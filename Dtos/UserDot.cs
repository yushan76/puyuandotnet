﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace PuyuanDotNet7.Dtos
{
    public class UserDto
    {
        [MaxLength(100)]
        [DefaultValue("root")]
        public string? Username { get; set; }

        [MaxLength(100)]
        [DefaultValue("2000/01/01")]
        public string? Birthday { get; set; }

        [DefaultValue(170)]
        public int? Height { get; set; }

        [DefaultValue(false)]
        public bool? Gender { get; set; }

        [MaxLength(100)]
        [DefaultValue("0")]
        public string? Fcm_id { get; set; }

        [MaxLength(100)]
        [DefaultValue("台中市")]
        public string? Address { get; set; }

        [DefaultValue(45)]
        public int? Weight { get; set; }

        [Phone]
        [MaxLength(100)]
        [DefaultValue("0987654321")]
        public string? Phone { get; set; }

        [EmailAddress]
        [MaxLength(100)]
        [DefaultValue("root@mail.com")]
        public string? Email { get; set; }

    }

}
