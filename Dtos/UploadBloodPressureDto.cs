using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace PuyuanDotNet7.Dtos
{
    public class UpdateBloodPresureDto
    {
        [MaxLength(50)]
        [DefaultValue(200)]
        public double Systolic { get; set; }
        [MaxLength(50)]
        [DefaultValue(110)]
        public double Diastolic { get; set; }
        [MaxLength(50)]
        [DefaultValue(120)]
        public int Pulse { get; set; }
        public DateTime recorded_at { get; set; }
    }
}
