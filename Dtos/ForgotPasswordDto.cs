﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace PuyuanDotNet7.Dtos
{
    public class ForgotPasswordDto
    {
        [Phone]
        [MaxLength(100)]
        [DefaultValue("0987654321")]
        public string? Phone { get; set; }

        [EmailAddress]
        [MaxLength(100)]
        [DefaultValue("root@mail.com")]
        public string? Email { get; set; }
    }
}