﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace PuyuanDotNet7.Dtos
{
    public class DefaultDto
    {
        [DefaultValue(1)]
        public int? sugar_delta_max { get; set; }

        [DefaultValue(1)]
        public int? sugar_delta_min { get; set; }

        [DefaultValue(1)]
        public int? sugar_morning_max { get; set; }

        [DefaultValue(1)]
        public int? sugar_morning_min { get; set; }

        [DefaultValue(1)]
        public int? sugar_evening_max { get; set; }

        [DefaultValue(1)]
        public int? sugar_evening_min { get; set; }

        [DefaultValue(1)]
        public int? sugar_before_max { get; set; }

        [DefaultValue(1)]
        public int? sugar_before_min { get; set; }

        [DefaultValue(1)]
        public int? sugar_after_max { get; set; }

        [DefaultValue(1)]
        public int? sugar_after_min { get; set; }

        [DefaultValue(1)]
        public int? systolic_max { get; set; }

        [DefaultValue(1)]
        public int? systolic_min { get; set; }

        [DefaultValue(1)]
        public int? diastolic_max { get; set; }

        [DefaultValue(1)]
        public int? diastolic_min { get; set; }

       
        [DefaultValue(1)]
        public int? pulse_max { get; set; }

        [DefaultValue(1)]
        public int? pulse_min { get; set; }

        [DefaultValue(1)]
        public int? weight_max { get; set; }

        [DefaultValue(1)]
        public int? weight_min { get; set; }

        [DefaultValue(1)]
        public int? bmi_max { get; set; }

        [MaxLength(100)]
        [DefaultValue(1)]
        public int? bmi_min { get; set; }

        [DefaultValue(1)]
        public int? body_fat_max { get; set; }

        [DefaultValue(1)]
        public int? body_fat_min { get; set; }
    }

}
