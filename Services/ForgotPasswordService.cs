﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Win32;
using PuyuanDotNet7.Data;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;

namespace PuyuanDotNet7.Services
{
    public class ForgotPasswordService
    {
        private readonly PuyuanContext _context;
        private readonly PasswordHelper _passwordHelper;
        private readonly JwtHelper _jwthelper;
        private readonly EmailSenderHelper _EmailSenderHelper;

        JsonResult success = new JsonResult(new { status = "0" });
        JsonResult fail = new JsonResult(new { status = "1" });

        public ForgotPasswordService(
            PuyuanContext context,
            PasswordHelper passwordHelper,
            JwtHelper jwtHelper,
            EmailSenderHelper EmailSenderHelper,
            IMapper mapper)
        {
            _context = context;
            _passwordHelper = passwordHelper;
            _jwthelper = jwtHelper;
            _EmailSenderHelper = EmailSenderHelper;
        }

        //private string HashPassword(string password)
        //{
        //    using (var sha256 = SHA256.Create())
        //    {
        //        var hashedBytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(password));
        //        return BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
        //    }
        //}
        public async Task<IActionResult> ForgotPassword(ForgotPasswordDto ForgotPasswordDto)
        {
            var user = _context.UserProfile
                .Include(e => e.UserSet).
                SingleOrDefault(e => e.Phone == ForgotPasswordDto.Phone && e.Email == ForgotPasswordDto.Email);
            if (user == null)
            {
                return fail;
            }
            var tempPassword = GenerateTempPassword();
            user.Password = _passwordHelper.HashPassword(tempPassword);

            //var message = new MailMessage();
            //message.To.Add(user.Email);
            //message.Subject = "Your temporary password";
            //message.Body = $"Your temporary password is: {tempPassword}";
            //using (var smtpClient = new SmtpClient())
            //{
            //    smtpClient.Send(message);
            //}
            var message = new MessageDto(
             ForgotPasswordDto.Email,
             "普元驗證密碼",
             $"Verification Code: {tempPassword}");
            try
            {
                _EmailSenderHelper.SendEmail(message);
            }
            catch
            {
                return fail;
            }
            user.UserSet.Must_Change_Password = true;
            _context.Entry(user).State = EntityState.Modified; //更新

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return fail;
            }
            return success;
        }
        private string GenerateTempPassword()
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()_+";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, 8).Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}