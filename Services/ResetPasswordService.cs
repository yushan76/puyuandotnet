﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Win32;
using PuyuanDotNet7.Data;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;

namespace PuyuanDotNet7.Services
{
    public class ResetPasswordService
    {
        private readonly PuyuanContext _context;
        private readonly PasswordHelper _passwordHelper;
        private readonly JwtHelper _jwthelper;
        private readonly EmailSenderHelper _EmailSenderHelper;

        JsonResult success = new JsonResult(new { status = "0" });
        JsonResult fail = new JsonResult(new { status = "1" });

        public ResetPasswordService(
            PuyuanContext context,
            PasswordHelper passwordHelper,
            JwtHelper jwtHelper,
            EmailSenderHelper EmailSenderHelper,
            IMapper mapper)
        {
            _context = context;
            _passwordHelper = passwordHelper;
            _jwthelper = jwtHelper;
            _EmailSenderHelper = EmailSenderHelper;
        }

        public async Task<IActionResult> ResetPassword(ResetPasswordDto ResetPasswordDto)
        {
            var user = _context.UserProfile
                .Include(e => e.UserSet).
                 SingleOrDefault(e => e.Email == ResetPasswordDto.Email && e.UserSet.Must_Change_Password == true);
            if (user == null)
            {
                return fail;
            }
            var passwordHasher = new PasswordHasher<ResetPasswordDto>();
            //var hashedPassword = passwordHasher.HashPassword(user.Password);
            user.Password = _passwordHelper.HashPassword(ResetPasswordDto.password);
            user.UserSet.Must_Change_Password = false;
            _context.Entry(user).State = EntityState.Modified;  //更新
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return fail;
            }
            return success;
        }
    }
}