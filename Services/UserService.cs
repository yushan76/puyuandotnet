﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Win32;
using PuyuanDotNet7.Data;

namespace PuyuanDotNet7.Services
{
    public class UserService
    {
        private readonly PuyuanContext _context;
        private readonly PasswordHelper _passwordHelper;
        private readonly JwtHelper _jwthelper;
        private readonly IMapper _mapper;

        JsonResult success = new JsonResult(new { status = "0" });
        JsonResult fail = new JsonResult(new { status = "1" });    

        public UserService(
            PuyuanContext context,
            PasswordHelper passwordHelper,
            JwtHelper jwtHelper,
            IMapper mapper)
        {
            _context = context;
            _passwordHelper = passwordHelper;
            _jwthelper = jwtHelper;
            _mapper = mapper;
        }
        public async Task<IActionResult> UserInfo(UserDto userDto, string uuid)
        {
            var user = _context.UserProfile
                .Include(e => e.UserSet)
                .SingleOrDefault(e => e.Uuid.Equals(uuid));
            Console.WriteLine(user.Uuid);
            if (user == null)
            {
                return fail;
            }
            user.UserSet = _mapper.Map(userDto, user.UserSet);
            user.Email = userDto.Email;
            user.Phone = userDto.Phone;
            _context.Entry(user).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return fail;
            }
            return success;
        }
    }
}