using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Win32;
using PuyuanDotNet7.Data;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;

namespace PuyuanDotNet7.Services
{
    public class UpdateBloodPressureService
    {
        private readonly PuyuanContext _Context;

        JsonResult success = new JsonResult(new { status = "0" });
        JsonResult fail = new JsonResult(new { status = "1" });

        public UpdateBloodPressureService(PuyuanContext puyuanContext)
        {
            _Context = puyuanContext;
        }
        public async Task<IActionResult> BloodPressure(UpdateBloodPresureDto updateBloodPresureDto, string UUID)
        {
            var user = _Context.BloodPressure.SingleOrDefault(e => e.Uuid.Equals(UUID));
            if (user == null)
            {
                return fail;
            }
            user.Systolic = updateBloodPresureDto.Systolic;
            user.Diastolic = updateBloodPresureDto.Diastolic;
            user.Pulse = updateBloodPresureDto.Pulse;
            user.Recorded_At = DateTime.UtcNow;
            try
            {
                _Context.BloodPressure.Update(user);
                await _Context.SaveChangesAsync();
            }
            catch
            {
                return fail;
            }
            return success;
        }
    }
}
