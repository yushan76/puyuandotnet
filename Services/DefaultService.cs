﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Win32;
using PuyuanDotNet7.Data;

namespace PuyuanDotNet7.Services
{
    public class DefaultService
    {
        private readonly PuyuanContext _context;
        private readonly PasswordHelper _passwordHelper;
        private readonly JwtHelper _jwthelper;
        private readonly IMapper _mapper;

        JsonResult success = new JsonResult(new { status = "0" });
        JsonResult fail = new JsonResult(new { status = "1" });

        public DefaultService(
            PuyuanContext context,
            PasswordHelper passwordHelper,
            JwtHelper jwtHelper,
            IMapper mapper)
        {
            _context = context;
            _passwordHelper = passwordHelper;
            _jwthelper = jwtHelper;
            _mapper = mapper;
        }
        public async Task<IActionResult> DefaultInfo(DefaultDto defaultDto, string uuid)
        {
            var user = _context.UserProfile
                .Include(e => e.Default)
                .SingleOrDefault(e => e.Uuid.Equals(uuid));
            Console.WriteLine(user.Uuid);
            if (user == null)
            {
                return fail;
            }
            user.Default = _mapper.Map(defaultDto, user.Default);
            _context.Entry(user).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
               return fail;
            }
            return success;
        }
    }
}