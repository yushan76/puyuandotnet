﻿using AutoMapper;
using Microsoft.AspNetCore.Routing.Constraints;
using PuyuanDotNet7.Dtos;
using System.Collections;

namespace PuyuanDotNet7.Profiles;

public class AutoMapperProfile : Profile
{
    public AutoMapperProfile()
    {

        CreateMap<RegisterDto, UserProfile>()
            .ForMember(x => x.Created_At, y => y.MapFrom(o => DateTime.Now))
            .ForMember(x => x.Password, y => y.Ignore());

        CreateMap<UserDto, UserSet>()
         .ForMember(x => x.Updated_At, y => y.MapFrom(o => DateTime.Now));

        CreateMap<DefaultDto, Default>()
        .ForMember(x => x.Updated_At, y => y.MapFrom(o => DateTime.Now));
    }
}
